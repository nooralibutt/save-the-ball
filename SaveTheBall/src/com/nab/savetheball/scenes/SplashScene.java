package com.nab.savetheball.scenes;

import org.cocos2d.actions.instant.CCCallFunc;
import org.cocos2d.actions.interval.CCDelayTime;
import org.cocos2d.actions.interval.CCSequence;
import org.cocos2d.layers.CCLayer;
import org.cocos2d.layers.CCScene;
import org.cocos2d.nodes.CCDirector;
import org.cocos2d.transitions.CCFadeTransition;

import com.nab.savetheball.utils.Utils;

public class SplashScene extends CCLayer {
	
	public static CCScene scene() {
		CCScene scene = CCScene.node();
		SplashScene layer = new SplashScene();
		scene.addChild(layer);
		return scene;
	}
	
	public SplashScene() {
		Utils.createBackground(this, "splash.png", -1);
	}
	
	@Override
	public void onEnter() {
		super.onEnter();
		
		this.runAction(CCSequence.actions(CCDelayTime.action(1.5f), CCCallFunc.action(this, "startGame")));
	}
	
	public void startGame(){
		CCFadeTransition ccFadeTransition = CCFadeTransition.transition(1,
				MainMenuScene.scene());
		CCDirector.sharedDirector().replaceScene(ccFadeTransition);
	}
}
