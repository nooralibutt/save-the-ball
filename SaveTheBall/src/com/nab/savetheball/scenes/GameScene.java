package com.nab.savetheball.scenes;

import java.util.ArrayList;
import org.cocos2d.actions.UpdateCallback;
import org.cocos2d.actions.base.CCRepeatForever;
import org.cocos2d.actions.instant.CCCallFunc;
import org.cocos2d.actions.instant.CCCallFuncN;
import org.cocos2d.actions.interval.CCDelayTime;
import org.cocos2d.actions.interval.CCRotateBy;
import org.cocos2d.actions.interval.CCScaleTo;
import org.cocos2d.actions.interval.CCSequence;
import org.cocos2d.config.ccMacros;
import org.cocos2d.layers.CCColorLayer;
import org.cocos2d.layers.CCScene;
import org.cocos2d.menus.CCMenu;
import org.cocos2d.menus.CCMenuItemSprite;
import org.cocos2d.nodes.CCDirector;
import org.cocos2d.nodes.CCSprite;
import org.cocos2d.types.CGPoint;
import org.cocos2d.types.CGRect;
import org.cocos2d.types.CGSize;
import org.cocos2d.types.ccColor3B;
import org.cocos2d.types.ccColor4B;

import android.util.Log;
import android.view.MotionEvent;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.EdgeShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.Manifold;
import com.badlogic.gdx.physics.box2d.World;
import com.nab.ads.AdManager;
import com.nab.savetheball.R;
import com.nab.savetheball.utils.Ball;
import com.nab.savetheball.utils.CCParticleCustom;
import com.nab.savetheball.utils.GameOverDialog;
import com.nab.savetheball.utils.TrailRenderer;
import com.nab.savetheball.utils.Utils;

public class GameScene extends CCColorLayer implements ContactListener {

	protected final World _world;
	protected static Body _body;

	/**
	 * current Time To Add Ball
	 */
	private float currentTime = 0;

	/**
	 * Total Time when ball will be added
	 */
	private float totalTime = 10;

	/**
	 * Total Elapsed game Time
	 */
	private float gameTime = 0;

	/**
	 * Player ball
	 */
	Ball playerBall;

	/**
	 * array of all enemy balls
	 */
	ArrayList<Ball> _balls;

	private float boxWidth;
	private float boxHeight;

	/**
	 * X position of Gameplay box
	 */
	private float boxXposition;
	/**
	 * Y position of Gameplay box
	 */
	private float boxYposition;

	/**
	 * progress bar sprite. shows the status of next enemy ball
	 */
	CCSprite progress;
	float progress_max_width;

	/**
	 * power up slow button
	 */
	private CCMenuItemSprite slowButton;
	private CCMenuItemSprite invisibleButton;

	private final float STROKE = 20.0f * Utils.sX;

	private TrailRenderer trailRenderer;
	private CCParticleCustom customParticle;

	private float redMagnitude;
	private float blueMagnitude;
	private float greenMagnitude;

	public static CCScene scene() {
		CCScene scene = CCScene.node();
		GameScene layer = new GameScene();
		scene.addChild(layer);
		return scene;
	}

	public GameScene() {
		super(ccColor4B.ccc4(137, 150, 170, 255));

		// Get window size
		CGSize s = CCDirector.sharedDirector().winSize();

		customParticle = CCParticleCustom.node(R.raw.balls_particles2);
		customParticle.setPosition(Utils.SC_W / 2, Utils.SC_H / 2);
		addChild(customParticle, -1);

		CCSprite borderSprite = Utils.createSprite(this, "game_box1.png", 0, 0,
				false);
		borderSprite.setAnchorPoint(0, 0);

		boxWidth = Utils.getScaledWidth(borderSprite);
		boxHeight = Utils.getScaledHeight(borderSprite);

		boxXposition = s.width / 2 - boxWidth / 2;
		boxYposition = s.height / 2 - boxHeight / 2;

		borderSprite.setPosition(boxXposition, boxYposition);

		// adding progress bar
		CCSprite progressBg = Utils.createSprite(this, "progress_bar_bg.png",
				0, 0);
		progressBg.setAnchorPoint(0, 0);
		progressBg.setPosition(boxXposition,
				boxYposition + boxHeight + Utils.getScaledHeight(progressBg)
						/ 4);

		progress = Utils.createSprite(this, "progress_bar.png", boxXposition,
				boxYposition + boxHeight + Utils.getScaledHeight(progressBg)
						/ 4);
		progress.setAnchorPoint(0, 0);
		progress_max_width = progress.getTextureRect().size.width;

		Utils.createSprite(this, "icon_progress_bar.png",
				progressBg.getPosition().x, progressBg.getPosition().y)
				.setAnchorPoint(0, 0);

		// adding power ups
		slowButton = Utils.createButton("icon_slow1.png", "icon_slow1.png", 0,
				0, this, "powerUpSlow");
		slowButton.setPosition(
				boxXposition + boxWidth - Utils.getScaledWidth(slowButton)
						/ 1.5f,
				boxYposition + boxHeight + Utils.getScaledHeight(slowButton)
						/ 2f);
		slowButton.runAction(CCRepeatForever.action(CCSequence.actions(
				CCDelayTime.action(5), Utils.getPromptAnimation(0.1f, 3),
				CCScaleTo.action(0.1f, Utils.sX, Utils.sY))));
		invisibleButton = Utils.createButton("icon_invisible.png",
				"icon_invisible.png", 0, 0, this, "powerUpInvisible");
		invisibleButton.runAction(CCRepeatForever.action(CCSequence.actions(
				CCDelayTime.action(5), Utils.getPromptAnimation(0.1f, 3),
				CCScaleTo.action(0.1f, Utils.sX, Utils.sY))));
		invisibleButton.setPosition(
				boxXposition + boxWidth - Utils.getScaledWidth(invisibleButton)
						* 2f,
				boxYposition + boxHeight
						+ Utils.getScaledHeight(invisibleButton) / 2f);

		CCMenu menu = CCMenu.menu(slowButton, invisibleButton);
		menu.setPosition(0, 0);
		addChild(menu);

		// Utils.createSprite(this, "slow.png", boxXposition
		// + boxWidth, boxYposition + boxHeight);
		// Create a world
		Vector2 gravity = new Vector2(0.0f, 0.0f);
		boolean doSleep = true;
		_world = new World(gravity, doSleep);
		_world.setContactListener(this);

		// Create edges around the entire screen
		// Define the ground body.
		BodyDef bxGroundBodyDef = new BodyDef();
		bxGroundBodyDef.position.set(0, 0);

		// The body is also added to the world.
		Body groundBody = _world.createBody(bxGroundBodyDef);

		// Define the ground box shape.
		EdgeShape groundEdge = new EdgeShape();
		FixtureDef boxShapeDef = new FixtureDef();
		boxShapeDef.shape = groundEdge;
		
//		PolygonShape groundBox = new PolygonShape();
		Vector2 bottomLeft = new Vector2((boxXposition + STROKE)
				* Utils.PTM_RATIO, (boxYposition + STROKE) * Utils.PTM_RATIO);
		Vector2 topLeft = new Vector2(
				(boxXposition + STROKE) * Utils.PTM_RATIO, (boxYposition
						+ boxHeight - STROKE)
						* Utils.PTM_RATIO);
		Vector2 topRight = new Vector2((boxXposition + boxWidth - STROKE)
				* Utils.PTM_RATIO, (boxYposition + boxHeight - STROKE)
				* Utils.PTM_RATIO);
		Vector2 bottomRight = new Vector2((boxXposition + boxWidth - STROKE)
				* Utils.PTM_RATIO, (boxYposition + STROKE) * Utils.PTM_RATIO);
		
		// bottom
		groundEdge.set(bottomLeft, bottomRight);
		groundBody.createFixture(boxShapeDef);
		// top
		groundEdge.set(topLeft, topRight);
		groundBody.createFixture(boxShapeDef);
		// left
		groundEdge.set(topLeft, bottomLeft);
		groundBody.createFixture(boxShapeDef);
		// right
		groundEdge.set(topRight, bottomRight);
		groundBody.createFixture(boxShapeDef);

		// adding player ball
		playerBall = new Ball(CGPoint.ccp(Utils.SC_W / 2, Utils.SC_H / 2),
				_world, true);
		addChild(playerBall);

		this.setIsTouchEnabled(true);

		// adding enemy balls
		_balls = new ArrayList<Ball>();
		Ball ball = new Ball(CGPoint.ccp(Utils.SC_W / 2, boxYposition
				+ boxHeight * 0.2f), _world, false);
		_balls.add(ball);
		addChild(ball);

		Ball ball1 = new Ball(CGPoint.ccp(Utils.SC_W / 2, boxYposition
				+ boxHeight * 0.8f), _world, false);
		_balls.add(ball1);
		addChild(ball1);

		// set Current time and total time to add ball
		currentTime = 0;
		totalTime = 10;

		// adding GameOver Dialog
		gameOverDialog = new GameOverDialog();
		addChild(gameOverDialog, 10000);

		// Enable accelerometer
		// this.setIsAccelerometerEnabled(true);

		trailRenderer = new TrailRenderer(this, 7, 0.2f);
	}

	@Override
	public void onEnter() {
		super.onEnter();

		AdManager.onPreLoad();

		Utils.setIntegerForKey("gamesPlayed",
				Utils.getIntegerForKey("gamesPlayed", 0) + 1);
		Utils.playBackgroundMusic(R.raw.music);

		// Schedule tick
		schedule(tickCallback);
	}

	@Override
	public void onEnterTransitionDidFinish() {
		super.onEnterTransitionDidFinish();

		showPopup("popup_balls_" + _balls.size() + ".png");
		AdManager.showBannerAd();
	}

	public void powerUpSlow(Object sender) {
		slowButton.stopAllActions();

		slowButton.setScaleX(Utils.sX);
		slowButton.setScaleY(Utils.sY);

		// adding slow popup
		showPopup("popup_slow.png");

		slowButton.runAction(CCRepeatForever.action(CCRotateBy
				.action(3.3f, 360)));
		slowButton.setIsEnabled(false);

		slowButton.runAction(CCSequence.actions(CCDelayTime.action(9.9f),
				CCCallFuncN.action(this, "lockPowerUp")));

		for (int i = 0; i < _balls.size(); i++) {
			_balls.get(i).decreaseSpeed(0.4f, 11);
		}

	}

	public void powerUpInvisible(Object sender) {
		invisibleButton.stopAllActions();

		invisibleButton.setScaleX(Utils.sX);
		invisibleButton.setScaleY(Utils.sY);

		// adding slow popup
		showPopup("popup_invisible.png");

		invisibleButton.runAction(CCRepeatForever.action(CCRotateBy.action(
				3.3f, 360)));
		invisibleButton.setIsEnabled(false);
		playerBall.ballBody.setActive(false);

		playerBall.ballSprite.setOpacity(150);

		invisibleButton.runAction(CCSequence.actions(CCDelayTime.action(9.9f),
				CCCallFuncN.action(this, "lockPowerUp")));
		this.runAction(CCSequence.actions(CCDelayTime.action(11f),
				CCCallFunc.action(this, "resetPlayerBall")));
	}

	public void resetPlayerBall() {
		playerBall.ballSprite.setOpacity(255);
		playerBall.ballBody.setActive(true);
	}

	public void lockPowerUp(Object sender) {
		((CCMenuItemSprite) sender).stopAllActions();
		((CCMenuItemSprite) sender).setOpacity(100);
	}

	private void changeColor(int red, int green, int blue) {

		ccColor3B color = this.getColor();
		redMagnitude = (float) Math.round((red - color.r) / 10.0f);
		greenMagnitude = (float) Math.round((green - color.g) / 10.0f);
		blueMagnitude = (float) Math.round((blue - color.b) / 10.0f);
		Log.i("NAB", "color: " + redMagnitude + "," + greenMagnitude + ","
				+ blueMagnitude);
		this.schedule("changeBackgroundColor", 0.1f);
		this.runAction(CCSequence.actions(CCDelayTime.action(1),
				CCCallFunc.action(this, "stopChangingBackgroundColor")));
	}

	public void changeBackgroundColor(float dt) {
		ccColor3B color = this.getColor();
		color.r += redMagnitude;
		color.g += greenMagnitude;
		color.b += blueMagnitude;
		this.setColor(color);
	}

	public void stopChangingBackgroundColor() {
		this.unschedule("changeBackgroundColor");

//		ccColor3B color = this.getColor();
//		Log.i("NAB", "color: " + color.r + "," + color.g + "," + color.b);
	}

	private Ball addBall() {

		float x = (Utils.rand() % boxWidth) + boxXposition;
		if (x > (boxXposition + boxWidth / 2))
			x -= Utils.getScaledWidth(playerBall.ballSprite);
		else
			x += Utils.getScaledWidth(playerBall.ballSprite);
		float y = (Utils.rand() % boxHeight) + boxYposition;
		if (y > (boxYposition + boxHeight / 2))
			y -= Utils.getScaledHeight(playerBall.ballSprite);
		else
			y += Utils.getScaledHeight(playerBall.ballSprite);
		Ball ball = new Ball(CGPoint.ccp(x, y), _world, false);
		_balls.add(ball);
		addChild(ball);

		changeColor(Utils.rand() % 50 + 160, Utils.rand() % 50 + 160,
				Utils.rand() % 50 + 160);
		return ball;
	}

	@Override
	public void ccAccelerometerChanged(float accelX, float accelY, float accelZ) {
		// Landscape values
		Vector2 gravity = new Vector2(accelY * 15, -accelX * 15);
		_world.setGravity(gravity);
	}

	@Override
	public boolean ccTouchesBegan(MotionEvent event) {
		// Convert to CGPoint
		CGPoint location = CCDirector.sharedDirector().convertToGL(
				CGPoint.make(event.getX(), event.getY()));

		handleBallTouch(location, false);
		return super.ccTouchesBegan(event);
	}

	@Override
	public boolean ccTouchesMoved(MotionEvent event) {

		// Convert to CGPoint
		CGPoint location = CCDirector.sharedDirector().convertToGL(
				CGPoint.make(event.getX(), event.getY()));

		handleBallTouch(location, true);

		return super.ccTouchesMoved(event);
	}

	private void handleBallTouch(CGPoint location, boolean showTrail) {

		float halfWidth = Utils.getScaledWidth(playerBall.ballSprite) / 3;
		float halfHeight = Utils.getScaledHeight(playerBall.ballSprite) / 3;
		// check if ball is moved or touched within gameplay box
		if (location.x < boxXposition + halfWidth + STROKE
				|| location.x > boxXposition + boxWidth - halfWidth - STROKE
				|| location.y < boxYposition + halfHeight + STROKE
				|| location.y > boxYposition + boxHeight - halfHeight - STROKE)
			return;

		if (showTrail)
			trailRenderer.render(playerBall.ballSprite.getPosition());

		Body b = playerBall.ballBody;
		b.setTransform(new Vector2(location.x * Utils.PTM_RATIO, location.y
				* Utils.PTM_RATIO), b.getAngle());

		// Synchronize the Sprites position and rotation with the
		// corresponding body
		CCSprite sprite = playerBall.ballSprite;
		sprite.setPosition(b.getPosition().x * Utils.MTP_RATIO,
				b.getPosition().y * Utils.MTP_RATIO);
		sprite.setRotation(-1.0f * ccMacros.CC_RADIANS_TO_DEGREES(b.getAngle()));
	}

	private UpdateCallback tickCallback = new UpdateCallback() {
		@Override
		public void update(float d) {
			tick(d);
		}
	};
	private GameOverDialog gameOverDialog;
	private CCSprite popupSprite;

	public synchronized void tick(float delta) {

		currentTime += delta;

		// updating game time
		gameTime += delta;

		// updating progress bar
		updateProgress();

		if (currentTime >= totalTime) {
			currentTime = 0;

			// double the total time to add ball
			// until u reach 4 balls then each ball will be added at constant
			// time
			if (_balls.size() < 4)
				totalTime *= 2;

			if (_balls.size() <= 10)
				showPopup("popup_balls_" + (_balls.size() + 1) + ".png");

			// adding ball
			addBall();
		}

		synchronized (_world) {
			_world.step(delta, 10, 10);
		}

		for (int i = 0; i < _balls.size(); i++) {
			_balls.get(i).update(delta);
		}

		playerBall.update(delta);
	}

	@Override
	public void beginContact(Contact contact) {

	}

	@Override
	public void endContact(Contact contact) {
		Object ballA = contact.getFixtureA().getBody().getUserData();
		Object ballB = contact.getFixtureB().getBody().getUserData();

		// balls and walls
		if (ballA != null && ballA instanceof Ball && ballB != null
				&& ballB instanceof Ball) {
			Utils.playEffectMusic(R.raw.ball_hit);

			if (ballA == playerBall || ballB == playerBall) {
				Log.e("endContact", "player balls contacted");
				gameEnd();
				return;
			}

			((Ball) ballA).resetSpeed();
			((Ball) ballB).resetSpeed();
		} else if (ballA != null && ballA instanceof Ball && ballB == null) {
			Utils.playEffectMusic(R.raw.wall_hit);
			((Ball) ballA).resetSpeed();
		} else if (ballA == null && ballB != null && ballB instanceof Ball) {
			Utils.playEffectMusic(R.raw.wall_hit);
			((Ball) ballB).resetSpeed();
		}
	}

	private void gameEnd() {

		Utils.vibrate(600);

		invisibleButton.stopAllActions();
		slowButton.stopAllActions();

		if (popupSprite != null) {
			popupSprite.removeFromParentAndCleanup(true);
			popupSprite = null;
		}

		// updating score on Google Play services
		Utils.mainActivity.submitScore(_balls.size());
		Utils.mainActivity.submitTime((int) gameTime);

		// updating Achievemnts
		Utils.mainActivity.checkForAchievments(_balls.size(), (int) gameTime,
				Utils.getIntegerForKey("gamesPlayed", 0));

		Utils.stopBackgroundMusic();
		_world.setContactListener(null);
		Vector2 gravity = new Vector2(-20.0f, -20.0f);
		_world.setGravity(gravity);

		this.setIsTouchEnabled(false);

		this.runAction(CCSequence.actions(CCDelayTime.action(1f),
				CCCallFunc.action(this, "showGameOverPOPUP")));
	}

	public void showGameOverPOPUP() {
		this.stopAllActions();
		this.unschedule(tickCallback);

		this.reorderChild(customParticle, 99);

		if (gameOverDialog == null) {
			gameOverDialog = new GameOverDialog();
			addChild(gameOverDialog, 100);
		}
		gameOverDialog.show(_balls.size(), (int) gameTime);
	}

	private void updateProgress() {
		float width = progress_max_width / totalTime;
		CGRect rcPrev = progress.getTextureRect();
		progress.setTextureRect(CGRect.make(rcPrev.origin.x, rcPrev.origin.y,
				width * currentTime, rcPrev.size.height));
	}

	private void showPopup(String filename) {

		if (popupSprite != null) {
			popupSprite.removeFromParentAndCleanup(true);
			popupSprite = null;
		}

		popupSprite = Utils.createSprite(this, filename, 0, 0);
		popupSprite.setPosition(Utils.SC_W / 2, boxYposition + boxHeight
				- Utils.getScaledHeight(popupSprite) * 1.5f);
		popupSprite.setOpacity(150);
		popupSprite.runAction(CCSequence.actions(
				Utils.getPromptAnimation(0.2f, 5), CCScaleTo.action(0.2f, 0)));
	}

	@Override
	public void preSolve(Contact contact, Manifold oldManifold) {
	}

	@Override
	public void postSolve(Contact contact, ContactImpulse impulse) {
	}
}
