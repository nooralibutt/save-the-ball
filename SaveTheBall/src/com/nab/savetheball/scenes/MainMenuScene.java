package com.nab.savetheball.scenes;

import org.cocos2d.layers.CCLayer;
import org.cocos2d.layers.CCScene;
import org.cocos2d.menus.CCMenu;
import org.cocos2d.menus.CCMenuItemSprite;
import org.cocos2d.menus.CCMenuItemToggle;
import org.cocos2d.nodes.CCDirector;
import org.cocos2d.nodes.CCLabel;
import org.cocos2d.nodes.CCSprite;
import org.cocos2d.transitions.CCFadeTransition;
import org.cocos2d.types.CGPoint;
import org.cocos2d.types.CGSize;
import org.cocos2d.types.ccColor3B;

import com.nab.ads.AdManager;
import com.nab.savetheball.R;
import com.nab.savetheball.utils.CCParticleCustom;
import com.nab.savetheball.utils.HelpDialog;
import com.nab.savetheball.utils.QuitDialog;
import com.nab.savetheball.utils.Utils;

public class MainMenuScene extends CCLayer {

	private CCMenu menuButtons;
	private QuitDialog quitDialog;
	private HelpDialog helpDialog;
	private CCLabel rankLabel;

	public static CCScene scene() {
		CCScene scene = CCScene.node();
		MainMenuScene layer = new MainMenuScene();
		scene.addChild(layer);
		return scene;
	}

	public void toggleMenu(boolean value) {
		menuButtons.setIsTouchEnabled(value);
	}

	public MainMenuScene() {

		// Get window size
		CGSize s = CGSize.make(Utils.SC_W, Utils.SC_H);

		Utils.createBackground(this, "main_background.png", -1);
		
		CCParticleCustom customParticle = CCParticleCustom.node(R.raw.balls_particles);
		customParticle.setPosition(Utils.SC_W/2, Utils.SC_H/2);
		addChild(customParticle);
		
		// placing title sprite
		CCSprite titleSprite = Utils.createSprite(this, "title.png", 0, 0);
		titleSprite.setPosition(s.width / 2,
				s.height - Utils.getScaledHeight(titleSprite) * 1.5f);

		// placing play button
		CCMenuItemSprite playButton = Utils.createButton(
				"btn_play.png",
				"btn_play_pressed.png",
				s.width / 2,
				titleSprite.getPosition().y
						- Utils.getScaledHeight(titleSprite) * 2, this,
				"play_selector");

		// placing leaderboard button
		CCMenuItemSprite leaderboardButton = Utils.createButton(
				"btn_leaderboard.png", "btn_leaderboard_pressed.png",
				s.width / 2,
				playButton.getPosition().y - Utils.getScaledHeight(playButton),
				this, "leaderboard_selector");
		// placing rateme button
		CCMenuItemSprite ratemeButton = Utils.createButton("btn_rateme.png",
				"btn_rateme_pressed.png", s.width / 2,
				playButton.getPosition().y - Utils.getScaledHeight(playButton)
						* 2, this, "rateme_selector");
		// placing quit button
		CCMenuItemSprite quitButton = Utils.createButton("btn_quit.png",
				"btn_quit_pressed.png", s.width / 2, playButton.getPosition().y
						- Utils.getScaledHeight(playButton) * 3, this,
				"quit_selector");

		// placing help button
		CCMenuItemSprite helpButton = Utils.createButton("icon_help.png",
				"icon_help_pressed.png", 0, 0, this, "help_selector");
		helpButton.setPosition(Utils.SC_W - Utils.getScaledWidth(helpButton),
				Utils.getScaledHeight(helpButton));

		// placing sound button
		CCMenuItemToggle soundToggleButton = Utils.createToggleButton(
				"icon_sound.png", "icon_sound_pressed.png", 0, 0, this,
				"sound_toggle_selector");
		soundToggleButton.setPosition(Utils.getScaledWidth(soundToggleButton),
				Utils.getScaledHeight(soundToggleButton));

		if (Utils.isSound == false)
			soundToggleButton.setSelectedIndex(1);

		menuButtons = CCMenu.menu(playButton, leaderboardButton, ratemeButton,
				quitButton, soundToggleButton, helpButton);
		menuButtons.setPosition(CGPoint.ccp(0, 0));

		addChild(menuButtons);
		
		rankLabel = Utils.createLabel(this, "World Rank: undefined ", 0, 0, 60,
				ccColor3B.ccc3(255, 255, 255));
		rankLabel.setPosition(Utils.SC_W / 2,
				Utils.SC_H - Utils.getScaledHeight(rankLabel));
	}

	@Override
	public void onEnter() {
		super.onEnter();

		AdManager.isFullScreenAdShowing = true;
		AdManager.isBannerAdShowing = true;
		AdManager.hideBannerAd();
		AdManager.showRandomFullScreenAd();
		
		Utils.playBackgroundMusic(R.raw.main_menu);
		menuButtons.setIsTouchEnabled(true);
		
		// Loading Rank
		Utils.mainActivity.loadRank(rankLabel);
	}

	public void play_selector(Object sender) {
		Utils.playButtonClickEffect();

		if (Utils.getBooleanForKey("isTutorialShowed")) {
			CCFadeTransition ccFadeTransition = CCFadeTransition.transition(2,
					GameScene.scene());
			CCDirector.sharedDirector().replaceScene(ccFadeTransition);
		}
		else{
			if (helpDialog == null) {
				helpDialog = new HelpDialog(this);
				addChild(helpDialog);
			}
			helpDialog.show();
		}
	}

	public void leaderboard_selector(Object sender) {
		Utils.playButtonClickEffect();
		
		Utils.mainActivity.showLeaderboard();
	}

	public void rateme_selector(Object sender) {
		Utils.playButtonClickEffect();
		Utils.rateUs();
	}

	public void quit_selector(Object sender) {
		Utils.playButtonClickEffect();
		if (quitDialog == null) {
			quitDialog = new QuitDialog(this);
			addChild(quitDialog);
		}
		quitDialog.show();
	}

	public void sound_toggle_selector(Object sender) {
		Utils.setSound(!Utils.isSound);
		Utils.playButtonClickEffect();
	}

	public void help_selector(Object sender) {
		Utils.playButtonClickEffect();
		if (helpDialog == null) {
			helpDialog = new HelpDialog(this);
			addChild(helpDialog);
		}
		helpDialog.show();
	}
}