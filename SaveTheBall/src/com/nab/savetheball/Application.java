package com.nab.savetheball;

import org.cocos2d.layers.CCScene;
import org.cocos2d.nodes.CCDirector;
import org.cocos2d.nodes.CCLabel;
import org.cocos2d.opengl.CCGLSurfaceView;

import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.games.Games;
import com.google.android.gms.games.Player;
import com.google.android.gms.games.leaderboard.LeaderboardVariant;
import com.google.android.gms.games.leaderboard.Leaderboards;
import com.google.android.gms.games.leaderboard.Leaderboards.LoadPlayerScoreResult;
import com.google.example.games.basegameutils.GameHelper;
import com.google.example.games.basegameutils.GameHelper.GameHelperListener;
import com.nab.ads.AdManager;
import com.nab.savetheball.scenes.SplashScene;
import com.nab.savetheball.utils.Utils;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;

public class Application extends Activity {

	static {
		System.loadLibrary("gdx");
	}

	private CCGLSurfaceView mGLSurfaceView;

	private static GameHelper mHelper;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON,
				WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

		mGLSurfaceView = new CCGLSurfaceView(this);
		CCDirector director = CCDirector.sharedDirector();
		director.attachInView(mGLSurfaceView);
		director.setDeviceOrientation(CCDirector.kCCDeviceOrientationPortrait);

		setContentView(R.layout.application);

		// show FPS
		CCDirector.sharedDirector().setDisplayFPS(false);

		// frames per second
		CCDirector.sharedDirector().setAnimationInterval(1.0f / 60.0f);

		// CCDirector.sharedDirector().setProjection(2);

		// create game helper with all APIs (Games, Plus, AppState):
		mHelper = new GameHelper(this, GameHelper.CLIENT_GAMES);
		mHelper.setup(listener);
		// mHelper.enableDebugLog(true);

		Utils.initParameters(this);

		// Create starting scene, add first layer as child
		CCScene scene = SplashScene.scene();

		// Lookup R.layout.main
		FrameLayout layout = (FrameLayout) findViewById(R.id.layout_main);
		layout.addView(mGLSurfaceView);

		CCDirector.sharedDirector().runWithScene(scene);

		AdManager.initialize(this);
	}

	private GameHelperListener listener = new GameHelper.GameHelperListener() {
		@Override
		public void onSignInSucceeded() {
			// handle sign-in succeess
			Log.i("Google Play Services", "Signin Succeeded");
		}

		@Override
		public void onSignInFailed() {
			// handle sign-in failure (e.g. show Sign In button)
			Log.e("Google Play Services", "Signin failed");
		}

	};

	public void submitTime(int time) {
		// if (mHelper.isSignedIn()) {
		try {
			Games.Leaderboards.submitScore(mHelper.getApiClient(),
					getString(R.string.leaderboard_top_time_reached), time);
		} catch (Exception x) {
		}
		// }
	}

	public void submitScore(int score) {
		// if (mHelper.isSignedIn()) {
		try {
			Games.Leaderboards.submitScore(mHelper.getApiClient(),
					getString(R.string.leaderboard_top_balls_reached), score);
		} catch (Exception x) {
		}
		// }
	}

	public void showLeaderboard() {
		if (mHelper.isSignedIn()) {
			startActivityForResult(
					Games.Leaderboards.getAllLeaderboardsIntent(mHelper
							.getApiClient()), 12345);
		} else {
			// start the sign-in flow
			mHelper.beginUserInitiatedSignIn();
		}
	}

	public void showAchievements() {
		if (mHelper.isSignedIn()) {
			startActivityForResult(
					Games.Achievements.getAchievementsIntent(mHelper
							.getApiClient()), 12345);
		} else {
			// start the sign-in flow
			mHelper.beginUserInitiatedSignIn();
		}
	}

	public static void unlockAchievement(String MY_ACHIEVEMENT_ID) {
		if (mHelper.isSignedIn()) {
			// call a Play Games services API method, for example:
			Games.Achievements
					.unlock(mHelper.getApiClient(), MY_ACHIEVEMENT_ID);
		}
	}

	@Override
	protected void onActivityResult(int request, int response, Intent data) {
		super.onActivityResult(request, response, data);
		mHelper.onActivityResult(request, response, data);
	}

	@Override
	protected void onStart() {
		mHelper.onStart(this);
		AdManager.onStart();

		super.onStart();
	}

	@Override
	public void onPause() {
		super.onPause();

		CCDirector.sharedDirector().onPause();
	}

	@Override
	public void onResume() {
		CCDirector.sharedDirector().onResume();
		super.onResume();
	}

	@Override
	protected void onStop() {
		mHelper.onStop();

		AdManager.onStop();
		super.onStop();
	}

	@Override
	public void onBackPressed() {

		if (AdManager.onBackPressed())
			return;
	}

	@Override
	public void onDestroy() {

		AdManager.onDestroy();

		CCDirector.sharedDirector().end();
		super.onDestroy();
	}

	public void checkForAchievments(int balls, int gameTime, int totalGames) {
		if (totalGames >= 10) {
			Application
					.unlockAchievement(getString(R.string.achievement_beginner));
		}
		if (totalGames >= 50) {
			Application
					.unlockAchievement(getString(R.string.achievement_mediocre));
		}
		if (totalGames >= 100) {
			Application
					.unlockAchievement(getString(R.string.achievement_expert));
		}
		if (balls >= 10) {
			Application
					.unlockAchievement(getString(R.string.achievement_ball_mover));
		}
		if (gameTime >= 200) {
			Application
					.unlockAchievement(getString(R.string.achievement_time_spender));
		}
	}

	public void loadRank(final CCLabel rankLabel) {
		// if (mHelper.isSignedIn()) {
		try {
			PendingResult<LoadPlayerScoreResult> leaderboardScore = Games.Leaderboards
					.loadCurrentPlayerLeaderboardScore(mHelper.getApiClient(),
							getString(R.string.leaderboard_top_balls_reached),
							LeaderboardVariant.TIME_SPAN_ALL_TIME,
							LeaderboardVariant.COLLECTION_PUBLIC);
			leaderboardScore
					.setResultCallback(new ResultCallback<Leaderboards.LoadPlayerScoreResult>() {

						@Override
						public void onResult(LoadPlayerScoreResult arg0) {
							if (arg0 == null) {
								return;
							}
							rankLabel.setString("World Rank: "
									+ arg0.getScore().getDisplayRank() + " ");
							Log.i("my rank is: ", arg0.getScore()
									.getDisplayRank() + " ");
						}
					});
		} catch (Exception x) {
		}
		// } else
		// rankLabel.setString("Login to see world rank");
	}
}

