package com.nab.savetheball.utils;

import org.cocos2d.actions.instant.CCCallFunc;
import org.cocos2d.actions.interval.CCDelayTime;
import org.cocos2d.actions.interval.CCMoveTo;
import org.cocos2d.actions.interval.CCScaleTo;
import org.cocos2d.actions.interval.CCSequence;
import org.cocos2d.actions.interval.CCSpawn;
import org.cocos2d.layers.CCLayer;
import org.cocos2d.menus.CCMenu;
import org.cocos2d.menus.CCMenuItemSprite;
import org.cocos2d.nodes.CCDirector;
import org.cocos2d.nodes.CCLabel;
import org.cocos2d.nodes.CCSprite;
import org.cocos2d.transitions.CCFadeTransition;
import org.cocos2d.types.CGPoint;
import org.cocos2d.types.ccColor3B;

import com.nab.ads.AdManager;
import com.nab.savetheball.scenes.GameScene;
import com.nab.savetheball.scenes.MainMenuScene;

public class GameOverDialog extends CCLayer {
	private float myScaleX;
	private float myScaleY;

	private CCMenu menuButtons;
	private CCLabel ballsLabel;
	private CCLabel timeLabel;
	private int balls;
	private CCSprite[] ballSprites;
	private int gameTime;
	private int currentTime;
	private int currentBalls;

	public GameOverDialog() {

		// blurring background
		Utils.createBackground(this, "blur.png");

		// placing dialog
		CCSprite dialog = Utils.createSprite(this, "dialog_gameover.png",
				Utils.SC_W / 2, Utils.SC_H / 2);

		// placing buttons
		float width = Utils.getScaledWidth(dialog);
		float height = Utils.getScaledHeight(dialog);

		placeBalls(width, height);

		width /= 2;
		height /= 2;

		CCMenuItemSprite shareButton = Utils.createButton(
				"btn_share_pressed.png", "btn_share.png", Utils.SC_W / 2 + 20,
				Utils.SC_H / 2 - height / 1.5f, this, "share_selector");

		CCMenuItemSprite facebookButton = Utils.createButton(
				"icon_facebook.png", "icon_facebook_pressed.png", Utils.SC_W
						/ 2 - Utils.getScaledWidth(shareButton) * 0.7f,
				Utils.SC_H / 2 - height / 1.5f, this, "facebook_selector");
		facebookButton.setScale(0.7f);

		CCMenuItemSprite reloadButton = Utils.createButton("icon_reload.png",
				"icon_reload_pressed.png", Utils.SC_W / 2 - width
						+ (width * 2 * 0.2f), Utils.SC_H / 2 - height, this,
				"reload_selector");

		CCMenuItemSprite leaderboardButton = Utils.createButton(
				"icon_leaderboard.png", "icon_leaderboard_pressed.png",
				Utils.SC_W / 2 - width + (width * 2 * 0.4f), Utils.SC_H / 2
						- height, this, "leaderboard_selector");

		CCMenuItemSprite achievementButton = Utils.createButton(
				"icon_achievement.png", "icon_achievement_pressed.png",
				Utils.SC_W / 2 - width + (width * 2 * 0.6f), Utils.SC_H / 2
						- height, this, "achievemnent_selector");

		CCMenuItemSprite menuButton = Utils.createButton("icon_menu.png",
				"icon_menu_pressed.png", Utils.SC_W / 2 - width
						+ (width * 2 * 0.8f), Utils.SC_H / 2 - height, this,
				"menu_selector");

		menuButtons = CCMenu.menu(reloadButton, menuButton, shareButton,
				leaderboardButton, facebookButton, achievementButton);
		menuButtons.setPosition(CGPoint.ccp(0, 0));
		addChild(menuButtons);

		ballsLabel = Utils.createLabel(this, "0 ", 0, 0, 60,
				ccColor3B.ccc3(248, 250, 250));
		ballsLabel.setPosition(Utils.SC_W / 2 + width / 2.5f, Utils.SC_H / 2
				- Utils.getScaledHeight(ballsLabel) / 2 + 10 * Utils.sY);
		ballsLabel.setAnchorPoint(0, 0.5f);

		timeLabel = Utils.createLabel(this, "0 ", 0, 0, 60,
				ccColor3B.ccc3(248, 250, 250));
		timeLabel.setPosition(Utils.SC_W / 2 + width / 2,
				ballsLabel.getPosition().y - Utils.getScaledHeight(ballsLabel));
		timeLabel.setAnchorPoint(0, 0.5f);

		// disabling all contents
		this.setVisible(false);
		menuButtons.setIsTouchEnabled(false);
		this.myScaleX = this.getScaleX();
		this.myScaleY = this.getScaleY();
		this.setScale(0);
	}

	private void placeBalls(float width, float height) {
		CCSprite dummySprite = Utils.createSprite(this, "enemy_ball.png", 0, 0);
		float widthOfBall = Utils.getScaledWidth(dummySprite);
		float heightOfBall = Utils.getScaledHeight(dummySprite);
		float paddingX = (float) Math.ceil(width / widthOfBall) + widthOfBall
				* 1.5f;
		float paddingY = (float) Math.ceil(height / heightOfBall)
				+ heightOfBall * 1.5f;

		float startX = Utils.SC_W / 2 - width / 2 + paddingX * 0.6f;
		float startY = Utils.SC_H / 2 + height / 2 - paddingY * 0.8f;

		ballSprites = new CCSprite[10];
		for (int i = 1; i <= 5; i++) {
			ballSprites[i - 1] = Utils.createSprite(this, "enemy_ball.png",
					startX + paddingX * i, startY - paddingY);
			ballSprites[i - 1].setOpacity(100);
		}

		for (int i = 1; i <= 5; i++) {
			ballSprites[i + 4] = Utils.createSprite(this, "enemy_ball.png",
					startX + paddingX * i, startY - paddingY * 2f);
			ballSprites[i + 4].setOpacity(100);
		}
		dummySprite.removeFromParentAndCleanup(true);
	}

	private void prepareAndShowAnimation() {
		int currentBalls = 10;
		if(balls < 10)
			currentBalls = balls;
		float ballWidth = Utils.getScaledWidth(ballSprites[0]);
		CCSprite[] ballsGained = new CCSprite[currentBalls];
		for (int i = 0; i < currentBalls; i++) {
			ballsGained[i] = Utils.createSprite(this, "enemy_ball.png",
					ballSprites[i].getPosition().x - ballWidth,
					ballSprites[i].getPosition().y);
			ballsGained[i].setScale(0);
		}

		float time = 0.1f;
		CCSequence[] actions = new CCSequence[currentBalls];
		for (int i = 0; i < currentBalls; i++) {
			CCMoveTo ccMoveTo = CCMoveTo.action(time,
					ballSprites[i].getPosition());
			CCSpawn ccSpawn = CCSpawn.actions(CCScaleTo.action(time,
					ballSprites[i].getScaleX(), ballSprites[i].getScaleY()),
					ccMoveTo);
			actions[i] = CCSequence.actions(CCDelayTime.action(0.3f * i),
					CCScaleTo.action(0, ballSprites[i].getScaleX() * 3,
							ballSprites[i].getScaleY() * 3), ccSpawn, Utils
							.getShakeAnimation(ballSprites[i].getPosition(),
									ballWidth / 3, 0.03f));
		}
		for (int i = 0; i < currentBalls; i++)
			ballsGained[i].runAction(actions[i]);

		ballsLabel.runAction(CCSequence.actions(
				CCDelayTime.action(0.3f * currentBalls),
				CCCallFunc.action(this, "scheduleBalls")));
	}

	public void scheduleBalls(){
		this.schedule("animateBall", 0.15f);
	}
	
	public void animateBall(float dt){
		if (currentBalls <= balls)
			ballsLabel.setString(currentBalls++ + " ");
		else{
			this.schedule("animateTime", 0.02f);
			this.unschedule("animateBall");
		}
	}

	public void animateTime(float dt){
		if (currentTime <= gameTime)
			timeLabel.setString(currentTime++ + " ");
		else
			this.unschedule("animateTime");
	}
	
	public void show(int balls, int gameTime) {

		this.balls = balls;
		this.gameTime = gameTime;
		this.prepareAndShowAnimation();

		// enabling popup
		this.setVisible(true);
		menuButtons.setIsTouchEnabled(true);
		this.runAction(CCSequence.actions(
				CCScaleTo.action(0.2f, myScaleX * 1.3f, myScaleY * 1.3f),
				CCScaleTo.action(0.2f, myScaleX, myScaleY)));
		
		AdManager.isFullScreenAdShowing = true;
		AdManager.showRandomFullScreenAd();
	}

	public void reload_selector(Object sender) {
		Utils.playButtonClickEffect();

		CCFadeTransition ccFadeTransition = CCFadeTransition.transition(2,
				GameScene.scene());
		CCDirector.sharedDirector().replaceScene(ccFadeTransition);
	}

	public void menu_selector(Object sender) {
		Utils.playButtonClickEffect();

		CCFadeTransition ccFadeTransition = CCFadeTransition.transition(2,
				MainMenuScene.scene());
		CCDirector.sharedDirector().replaceScene(ccFadeTransition);

		Utils.AppRater();
	}

	public void share_selector(Object sender) {
		Utils.playButtonClickEffect();

		Utils.share();
	}

	public void leaderboard_selector(Object sender) {
		Utils.playButtonClickEffect();

		Utils.mainActivity.showLeaderboard();
	}

	public void achievemnent_selector(Object sender) {
		Utils.playButtonClickEffect();

		Utils.mainActivity.showAchievements();
	}

	public void facebook_selector(Object sender) {
		Utils.playButtonClickEffect();

		// update icon of share on facebook server and in code
		Utils.shareFacebook(balls);
	}
}
