package com.nab.savetheball.utils;

import org.cocos2d.actions.interval.CCFadeOut;
import org.cocos2d.nodes.CCNode;
import org.cocos2d.nodes.CCSprite;
import org.cocos2d.types.CGPoint;

public class TrailRenderer {

	private CCSprite[] objects;
	private CCFadeOut[] fadeOutAnimations;

	private int currentlyRendering;

	public TrailRenderer(CCNode target, int capacity, float pFadeTime) {

		objects = new CCSprite[capacity];
		for (int i = 0; i < capacity; i++) {
			objects[i] = Utils.createSprite(target, "ball.png", 0, 0, 1000);
			objects[i].setScale(objects[i].getScale() * 0.5f);
			objects[i].setOpacity(0);
		}

		fadeOutAnimations = new CCFadeOut[capacity];
		for (int i = 0; i < capacity; i++) {
			fadeOutAnimations[i] = CCFadeOut.action(pFadeTime);
		}

		currentlyRendering = 0;
	}

	public void render(CGPoint position) {

		objects[currentlyRendering].setPosition(position);
		objects[currentlyRendering]
				.runAction(fadeOutAnimations[currentlyRendering]);

		currentlyRendering++;
		
		currentlyRendering %= objects.length;
		// Utils.createSprite(target, "ball.png", target1.getPosition().x,
		// target1.getPosition().y,
		// target1.getScaleX() * 0.5f, target1.getScaleY() * 0.5f, 1000);
	}
}
