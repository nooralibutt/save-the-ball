package com.nab.savetheball.utils;

import org.cocos2d.actions.instant.CCCallFunc;
import org.cocos2d.actions.interval.CCDelayTime;
import org.cocos2d.actions.interval.CCFadeIn;
import org.cocos2d.actions.interval.CCSequence;
import org.cocos2d.config.ccMacros;
import org.cocos2d.nodes.CCNode;
import org.cocos2d.nodes.CCSprite;
import org.cocos2d.types.CGPoint;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;

public class Ball extends CCNode {

	private final float SPEED_KOEF = 8.0f;
	
	private float current_speed_koef = SPEED_KOEF;

	public CCSprite ballSprite;
	public Body ballBody;

	private World _world;

	public Ball(CGPoint position, World _world, boolean isPlayer) {
		// Create sprite and add it to the layer
		if (isPlayer)
			ballSprite = Utils.createSprite(this, "ball.png", position.x,
					position.y);
		else {
			ballSprite = Utils.createSprite(this, "enemy_ball.png", position.x,
					position.y);
			ballSprite.setOpacity(0);
			ballSprite.runAction(CCSequence.actions(CCFadeIn.action(2),
					CCCallFunc.action(this, "initializeBody")));
		}	
		ballSprite.setScaleY(ballSprite.getScaleX());
		
		this._world = _world;
		if (isPlayer) {
			// Create ball body and shape
			BodyDef ballBodyDef = new BodyDef();
			ballBodyDef.type = BodyType.DynamicBody;
			ballBodyDef.position.set(position.x * Utils.PTM_RATIO, position.y
					* Utils.PTM_RATIO);

			// The body is also added to the world.
			ballBody = _world.createBody(ballBodyDef);
			ballBody.setUserData(this);

			CircleShape circle = new CircleShape();
			circle.setRadius(Utils.getScaledWidth(ballSprite) * 0.5f
					* Utils.PTM_RATIO);

			FixtureDef ballShapeDef = new FixtureDef();
			ballShapeDef.shape = circle;
			ballShapeDef.density = 10.0f;
			ballShapeDef.friction = 0f;
			ballShapeDef.restitution = 0f;
			ballBody.createFixture(ballShapeDef);
		}
	}

	public void initializeBody() {
		// Create ball body and shape
		BodyDef ballBodyDef = new BodyDef();
		ballBodyDef.type = BodyType.DynamicBody;
		ballBodyDef.position.set(ballSprite.getPosition().x * Utils.PTM_RATIO,
				ballSprite.getPosition().y * Utils.PTM_RATIO);

		// The body is also added to the world.
		ballBody = _world.createBody(ballBodyDef);
		ballBody.setUserData(this);

		CircleShape circle = new CircleShape();
		circle.setRadius(Utils.getScaledWidth(ballSprite) * 0.5f
				* Utils.PTM_RATIO);

		FixtureDef ballShapeDef = new FixtureDef();
		ballShapeDef.shape = circle;
		ballShapeDef.density = 10.0f;
		ballShapeDef.friction = 0f;
		ballShapeDef.restitution = 1f;
		ballBody.createFixture(ballShapeDef);
		move();
	}

	private void move() {
		float angle = (float) ((Utils.rand() % 360) * Math.PI / 180);
		ballBody.setLinearVelocity(new Vector2(
				(float) (Math.cos(angle) * current_speed_koef
						* ballSprite.getContentSize().width * Utils.PTM_RATIO),
				(float) (Math.sin(angle) * current_speed_koef
						* ballSprite.getContentSize().width * Utils.PTM_RATIO)));
	}

	public void increaseSpeed() {
		current_speed_koef++;
		resetSpeed();
	}

	public void decreaseSpeed(float percentage, int seconds) {
		current_speed_koef *= percentage;
		resetSpeed();
		this.runAction(CCSequence.actions(CCDelayTime.action(seconds),
				CCCallFunc.action(this, "resetKOEF")));
	}

	public void resetKOEF(){
		current_speed_koef = SPEED_KOEF;
		resetSpeed();
	}
	
	public void resetSpeed() {
		if (ballBody == null || ballBody.getLinearVelocity().len() < 1f)
			return;
		float speedKoef = ballSprite.getContentSize().width * current_speed_koef
				* Utils.PTM_RATIO / ballBody.getLinearVelocity().len();
		ballBody.setLinearVelocity(new Vector2(ballBody.getLinearVelocity().x
				* speedKoef, ballBody.getLinearVelocity().y * speedKoef));
	}

	public void update(float dt) {
		if (ballBody == null)
			return;
		Vector2 velocity = ballBody.getLinearVelocity();
		ballBody.setTransform(ballBody.getPosition(),
				(float) Math.atan2(velocity.y, velocity.x));

		// Synchronize the Sprites position and rotation with the
		// corresponding body
		ballSprite.setPosition(ballBody.getPosition().x * Utils.MTP_RATIO,
				ballBody.getPosition().y * Utils.MTP_RATIO);
		ballSprite.setRotation(-1.0f
				* ccMacros.CC_RADIANS_TO_DEGREES(ballBody.getAngle()));
	}
}
