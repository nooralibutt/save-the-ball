package com.nab.savetheball.utils;

import org.cocos2d.actions.interval.CCScaleTo;
import org.cocos2d.actions.interval.CCSequence;
import org.cocos2d.layers.CCLayer;
import org.cocos2d.menus.CCMenu;
import org.cocos2d.menus.CCMenuItemSprite;
import org.cocos2d.nodes.CCDirector;
import org.cocos2d.nodes.CCSprite;
import org.cocos2d.transitions.CCFadeTransition;
import org.cocos2d.types.CGPoint;

import com.nab.savetheball.scenes.GameScene;
import com.nab.savetheball.scenes.MainMenuScene;

public class HelpDialog extends CCLayer {

	float myScaleX;
	float myScaleY;

	MainMenuScene mainMenuScene;
	private CCMenu menuButtons;

	public HelpDialog(MainMenuScene mainMenuScene) {
		this.mainMenuScene = mainMenuScene;

		// blurring background
		Utils.createBackground(this, "blur.png");

		// placing dialog
		CCSprite dialog = Utils.createSprite(this, "dialog_tutorial.png",
				Utils.SC_W / 2, Utils.SC_H / 2);

		// placing buttons
		float height = Utils.getScaledHeight(dialog) / 2;
		CCMenuItemSprite acceptButton = Utils.createButton("icon_accept.png",
				"icon_accept_pressed.png", Utils.SC_W / 2, Utils.SC_H / 2
						- height, this, "quit_accept_selector");

		menuButtons = CCMenu.menu(acceptButton);
		menuButtons.setPosition(CGPoint.ccp(0, 0));
		addChild(menuButtons);

		// disabling all contents
		this.setVisible(false);
		menuButtons.setIsTouchEnabled(false);
		this.myScaleX = this.getScaleX();
		this.myScaleY = this.getScaleY();
		this.setScale(0);
	}

	public void show() {
		// disabling main menu layer
		this.mainMenuScene.toggleMenu(false);

		// enabling popup contents
		this.setVisible(true);
		menuButtons.setIsTouchEnabled(true);
		this.runAction(CCSequence.actions(
				CCScaleTo.action(0.2f, myScaleX * 1.3f, myScaleY * 1.3f),
				CCScaleTo.action(0.2f, myScaleX, myScaleY)));
	}

	public void quit_accept_selector(Object sender) {
		Utils.playButtonClickEffect();

		this.stopAllActions();
		menuButtons.setIsTouchEnabled(false);
		this.setVisible(false);
		this.setScale(0);

		if (Utils.getBooleanForKey("isTutorialShowed") == false) {
			Utils.setBooleanForKey("isTutorialShowed", true);
			
			CCFadeTransition ccFadeTransition = CCFadeTransition.transition(2,
					GameScene.scene());
			CCDirector.sharedDirector().replaceScene(ccFadeTransition);
		} else
			// enabling main menu layer
			this.mainMenuScene.toggleMenu(true);
	}
}
