package com.nab.savetheball.utils;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.IntBuffer;
import java.util.List;
import java.util.Locale;
import java.util.Random;

import javax.microedition.khronos.opengles.GL10;

import org.cocos2d.actions.interval.CCMoveTo;
import org.cocos2d.actions.interval.CCRepeat;
import org.cocos2d.actions.interval.CCScaleTo;
import org.cocos2d.actions.interval.CCSequence;
import org.cocos2d.menus.CCMenuItemSprite;
import org.cocos2d.menus.CCMenuItemToggle;
import org.cocos2d.nodes.CCAnimation;
import org.cocos2d.nodes.CCDirector;
import org.cocos2d.nodes.CCLabel;
import org.cocos2d.nodes.CCNode;
import org.cocos2d.nodes.CCSprite;
import org.cocos2d.nodes.CCSpriteFrame;
import org.cocos2d.nodes.CCSpriteFrameCache;
import org.cocos2d.types.CGPoint;
import org.cocos2d.types.CGSize;
import org.cocos2d.types.ccColor3B;

import com.facebook.widget.FacebookDialog;
import com.nab.savetheball.Application;
import com.nab.savetheball.R;
import com.sbstrm.appirater.Appirater;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.net.Uri;
import android.os.Environment;
import android.util.DisplayMetrics;
import android.util.Log;
import android.os.Vibrator;

public class Utils {
	// public static boolean isMusic = true;
	public static boolean isSound = true;

	public static Application mainActivity = null;
	public static SharedPreferences preference = null;
	public static Editor editor = null;

	public static float SC_W = 0.0f;
	public static float SC_H = 0.0f;

	public static float sX = 1;
	public static float sY = 1;

	/**
	 * defining a ratio of "pixels" to "meters"
	 */
	public static final float PTM_RATIO = 1 / 32.0f;

	/**
	 * defining a ratio of "meters" to "pixels"
	 */
	public static final float MTP_RATIO = 32.0f;

	public static void initParameters(Application mainActivity_) {
		mainActivity = mainActivity_;
		preference = mainActivity.getPreferences(0);
		editor = preference.edit();

		DisplayMetrics displayMetrics = new DisplayMetrics();
		mainActivity.getWindowManager().getDefaultDisplay()
				.getMetrics(displayMetrics);

		CGSize winSize = CGSize.make(displayMetrics.widthPixels,
				displayMetrics.heightPixels);

		SC_W = winSize.width;
		SC_H = winSize.height;

		Log.i("SaveTheBall", "Screen Width: " + SC_W);
		Log.i("SaveTheBall", "Screen Height: " + SC_H);

		CGSize winScale = CGSize.make(winSize.width / 720,
				winSize.height / 1280);

		sX = winScale.width;
		sY = winScale.height;

		SoundEngine.sharedEngine().setSoundVolume(0.7f);
		SoundEngine.sharedEngine().setEffectsVolume(1.0f);

		SoundEngine.sharedEngine().preloadSound(mainActivity, R.raw.music);
		SoundEngine.sharedEngine().preloadSound(mainActivity, R.raw.main_menu);

		SoundEngine.sharedEngine().preloadEffect(mainActivity, R.raw.ball_hit);
		SoundEngine.sharedEngine().preloadEffect(mainActivity, R.raw.wall_hit);
		SoundEngine.sharedEngine().preloadEffect(mainActivity, R.raw.button);

		CCSpriteFrameCache.sharedSpriteFrameCache().addSpriteFrames(
				"spritesheet0.plist");
		CCSpriteFrameCache.sharedSpriteFrameCache().addSpriteFrames(
				"spritesheet1.plist");
		CCSpriteFrameCache.sharedSpriteFrameCache().addSpriteFrames(
				"spritesheet2.plist");

		isSound = getSound();
		Utils.setSound(isSound);
	}

	public static CCMenuItemSprite createButton(String selectedImage,
			String normalImage, float x, float y, CCNode target, String selector) {

		CCSpriteFrame selectedFrame = CCSpriteFrameCache
				.sharedSpriteFrameCache().getSpriteFrame(selectedImage);
		CCSpriteFrame normalFrame = CCSpriteFrameCache.sharedSpriteFrameCache()
				.getSpriteFrame(normalImage);

		CCSprite spriteOn = CCSprite.sprite(normalFrame);

		CCSprite spriteOff = CCSprite.sprite(selectedFrame);

		CCMenuItemSprite button = CCMenuItemSprite.item(spriteOn, spriteOff,
				target, selector);

		button.setScaleX(sX);
		button.setScaleY(sY);

		button.setPosition(CGPoint.ccp(x, y));

		return button;
	}

	public static CCSprite createBackground(CCNode target, String fileName) {

		CCSprite pSprite = CCSprite.sprite(fileName);

		// position the sprite on the center of the screen
		pSprite.setPosition(CGPoint.ccp(SC_W / 2, SC_H / 2));

		pSprite.setScaleX(sX);
		pSprite.setScaleY(sY);

		// add the sprite as a child to this layer
		target.addChild(pSprite);
		return pSprite;
	}

	public static CCSprite createBackground(CCNode target, String fileName,
			int zValue) {

		CCSprite pSprite = CCSprite.sprite(fileName);

		// position the sprite on the center of the screen
		pSprite.setPosition(CGPoint.ccp(SC_W / 2, SC_H / 2));

		pSprite.setScaleX(sX);
		pSprite.setScaleY(sY);

		// add the sprite as a child to this layer
		target.addChild(pSprite, zValue);
		return pSprite;
	}

	public static CCSprite createSprite(CCNode target, String fileName,
			float x, float y) {

		CCSpriteFrame spriteFrame = CCSpriteFrameCache.sharedSpriteFrameCache()
				.getSpriteFrame(fileName);
		CCSprite pSprite = CCSprite.sprite(spriteFrame);

		// position the sprite on the center of the screen
		pSprite.setPosition(CGPoint.ccp(x, y));

		pSprite.setScaleX(sX);
		pSprite.setScaleY(sY);

		// add the sprite as a child to this layer
		target.addChild(pSprite);
		return pSprite;
	}

	public static CCSprite createSprite(CCNode target, String fileName,
			float x, float y, boolean isSpriteFrame) {

		CCSprite pSprite = null;
		if (isSpriteFrame) {
			CCSpriteFrame spriteFrame = CCSpriteFrameCache
					.sharedSpriteFrameCache().getSpriteFrame(fileName);
			pSprite = CCSprite.sprite(spriteFrame);
		} else {
			pSprite = CCSprite.sprite(fileName);
		}

		// position the sprite on the center of the screen
		pSprite.setPosition(CGPoint.ccp(x, y));

		pSprite.setScaleX(sX);
		pSprite.setScaleY(sY);

		// add the sprite as a child to this layer
		target.addChild(pSprite);
		return pSprite;
	}

	public static CCSprite createSprite(CCNode target, String fileName,
			float x, float y, int ZPostion) {

		CCSpriteFrame spriteFrame = CCSpriteFrameCache.sharedSpriteFrameCache()
				.getSpriteFrame(fileName);
		CCSprite pSprite = CCSprite.sprite(spriteFrame);

		// position the sprite on the center of the screen
		pSprite.setPosition(CGPoint.ccp(x, y));

		pSprite.setScaleX(sX);
		pSprite.setScaleY(sY);

		// add the sprite as a child to this layer
		target.addChild(pSprite, ZPostion);
		return pSprite;
	}

	public static CCSprite createSprite(CCNode target, String fileName,
			float x, float y, float _sX, float _sY, int zValue) {
		CCSpriteFrame spriteFrame = CCSpriteFrameCache.sharedSpriteFrameCache()
				.getSpriteFrame(fileName);
		CCSprite pSprite = CCSprite.sprite(spriteFrame);

		// position the sprite on the center of the screen
		pSprite.setPosition(CGPoint.ccp(x, y));

		pSprite.setScaleX(_sX);
		pSprite.setScaleY(_sY);

		// add the sprite as a child to this layer
		target.addChild(pSprite, zValue);
		return pSprite;
	}

	public static CCLabel createLabel(CCNode target, String text, float x,
			float y, int font_size, ccColor3B color) {
		CCLabel label = CCLabel.makeLabel(text, "bangers.ttf", font_size * sX);

		label.setPosition(CGPoint.ccp(x, y));
		label.setColor(color);
		target.addChild(label);
		return label;
	}

	public static CCMenuItemToggle createToggleButton(String fileName,
			String fileName2, float x, float y, CCNode target, String selector) {
		CCSpriteFrame spriteFrame = CCSpriteFrameCache.sharedSpriteFrameCache()
				.getSpriteFrame(fileName);
		CCSprite spriteOn = CCSprite.sprite(spriteFrame);

		CCSprite spriteOff = CCSprite.sprite(spriteFrame);
		spriteOff.setOpacity(100);
		CCMenuItemSprite button1 = CCMenuItemSprite.item(spriteOn, spriteOff);

		CCSpriteFrame spriteFrame2 = CCSpriteFrameCache
				.sharedSpriteFrameCache().getSpriteFrame(fileName2);
		spriteOn = CCSprite.sprite(spriteFrame2);

		spriteOff = CCSprite.sprite(spriteFrame2);
		spriteOff.setOpacity(100);

		CCMenuItemSprite button2 = CCMenuItemSprite.item(spriteOn, spriteOff);

		CCMenuItemToggle button = CCMenuItemToggle.item(target, selector,
				button1, button2);
		button.setScaleX(sX);
		button.setScaleY(sY);
		button.setPosition(CGPoint.ccp(x, y));

		return button;
	}

	public static void setBooleanForKey(String key, boolean value) {
		editor.putBoolean(key, value);
		editor.commit();
	}

	public static boolean getBooleanForKey(String key) {
		return preference.getBoolean(key, false);
	}

	public static void setIntegerForKey(String key, int value) {
		editor.putInt(key, value);
		editor.commit();
	}

	public static int getIntegerForKey(String key, int value) {
		return preference.getInt(key, value);
	}

	/**
	 * returns absolute(>=0) random integer number
	 * 
	 * @return int
	 */
	public static int rand() {
		Random r = new Random();
		return Math.abs(r.nextInt());
	}

	public static float getScaledWidth(CCSprite sprite) {
		return sprite.getContentSize().width * sprite.getScaleX();
	}

	public static float getScaledWidth(CCMenuItemToggle sprite) {
		return sprite.getContentSize().width * sprite.getScaleX();
	}

	public static float getScaledWidth(CCLabel label) {
		return label.getContentSize().width * label.getScaleX();
	}

	public static float getScaledWidth(CCMenuItemSprite sprite) {
		return sprite.getContentSize().width * sprite.getScaleX();
	}

	public static float getScaledHeight(CCSprite sprite) {
		return sprite.getContentSize().height * sprite.getScaleY();
	}

	public static float getScaledHeight(CCMenuItemToggle sprite) {
		return sprite.getContentSize().height * sprite.getScaleY();
	}

	public static float getScaledHeight(CCMenuItemSprite sprite) {
		return sprite.getContentSize().height * sprite.getScaleY();
	}

	public static CCSequence getShakeAnimation(CGPoint position, float shake,
			float time) {
		CCMoveTo move1 = CCMoveTo.action(time,
				CGPoint.make(position.x - shake, position.y));
		CCMoveTo move2 = CCMoveTo.action(time,
				CGPoint.make(position.x + shake, position.y));
		CCMoveTo move3 = CCMoveTo.action(time,
				CGPoint.make(position.x - shake, position.y));
		CCMoveTo move4 = CCMoveTo.action(time,
				CGPoint.make(position.x + shake, position.y));
		CCMoveTo move5 = CCMoveTo.action(time,
				CGPoint.make(position.x, position.y));

		CCSequence action = CCSequence.actions(move1, move2, move3, move4,
				move5);

		return action;
	}

	public static CCAnimation createAnimationWithPrefix(String prefix,
			int first, int last) {
		CCAnimation ani = CCAnimation.animation("character");
		for (int i = first; i <= last; i++) {
			String str_name = String.format(Locale.ENGLISH, "%s%02d.png",
					prefix, i);

			CCSpriteFrame frame = CCSpriteFrameCache.sharedSpriteFrameCache()
					.spriteFrameByName(str_name);
			if (frame != null) {
				ani.addFrame(frame);
			} else {
				break;
			}
		}

		return ani;
	}

	public static void playBackgroundMusic(int resId) {
		SoundEngine ag = SoundEngine.sharedEngine();
		ag.playSound(mainActivity, resId, true);
	}

	public static void pauseBackgroundMusic() {
		SoundEngine ag = SoundEngine.sharedEngine();
		ag.pauseSound();
	}

	public static void resumeBackgroundMusic() {
		SoundEngine ag = SoundEngine.sharedEngine();
		ag.resumeSound();
	}

	public static CCRepeat getPromptAnimation(float delay, int count) {
		CCScaleTo scaleUpAction = CCScaleTo.action(delay, Utils.sX * 1.3f,
				Utils.sY * 1.3f);
		CCScaleTo scaleDownAction = CCScaleTo.action(delay, Utils.sX * 0.7f,
				Utils.sY * 0.7f);
		CCRepeat scaleAnimattion = CCRepeat.action(
				CCSequence.actions(scaleUpAction, scaleDownAction), 3);
		return scaleAnimattion;
	}

	public static void stopBackgroundMusic() {
		SoundEngine ag = SoundEngine.sharedEngine();
		ag.stopSound();
	}

	public static void setBackgroundVolume(float volume) {
		SoundEngine.sharedEngine().setSoundVolume(volume);
	}

	public static void playEffectMusic(int resId) {
		SoundEngine ag = SoundEngine.sharedEngine();
		ag.playEffect(mainActivity, resId);
	}

	public static void playButtonClickEffect() {
		playEffectMusic(R.raw.button);
	}

	public static void resetSetting() {
		setSound(false);
		// setMusic(false);
	}

	public static boolean isPurchased(int item) {
		return getBooleanForKey(String.format(Locale.ENGLISH, "item%d", item));
	}

	public static void setPurchased(int item, boolean purchased) {
		setBooleanForKey(String.format(Locale.ENGLISH, "item%d", item),
				purchased);
	}

	public static void setSound(boolean value) {
		setBooleanForKey("sound", value);
		isSound = value;
		SoundEngine ag = SoundEngine.sharedEngine();
		if (value) {
			ag.unmute();
		} else {
			ag.mute();
		}
	}

	public static boolean getSound() {
		return preference.getBoolean("sound", true);
	}

	public static float getCurrentPoint() {
		return preference.getFloat("point", 0f);
	}

	public static void setCurrentPoint(float point) {
		setFloatForKey("point", point);
	}

	public static void setFloatForKey(String key, float value) {
		editor.putFloat(key, value);
		editor.commit();

	}

	public static void rateUs() {
		mainActivity.runOnUiThread(new Runnable() {
			@Override
			public void run() {

				String packageName = mainActivity.getPackageName();
				try {
					mainActivity.startActivity(new Intent(Intent.ACTION_VIEW,
							Uri.parse("market://details?id=" + packageName)));
				} catch (android.content.ActivityNotFoundException anfe) {
					mainActivity.startActivity(new Intent(
							Intent.ACTION_VIEW,
							Uri.parse("https://play.google.com/store/apps/details?id="
									+ packageName)));
				}
			}
		});

	}

	public static void AppRater() {
		mainActivity.runOnUiThread(new Runnable() {
			@Override
			public void run() {
				Appirater.appLaunched(mainActivity);
			}
		});

	}

	public static void shareTwitter(final int m_iScore) {

		CCDirector.sharedDirector();
		SavePNG(0, (int) (Utils.SC_H * (2.0 / 10.0)), (int) Utils.SC_W,
				(int) (Utils.SC_H * (8.0 / 10.0)), "/screenshot.png",
				CCDirector.gl);

		mainActivity.runOnUiThread(new Runnable() {
			@Override
			public void run() {

				Intent shareIntent = new Intent(
						android.content.Intent.ACTION_SEND);
				shareIntent.setType("image/png");
				shareIntent.putExtra(android.content.Intent.EXTRA_SUBJECT,
						"My Score Image");
				String packageName = mainActivity.getPackageName();
				shareIntent.putExtra(android.content.Intent.EXTRA_TEXT,
						"https://play.google.com/store/apps/details?id="
								+ packageName);
				shareIntent.putExtra(
						Intent.EXTRA_STREAM,
						Uri.parse("file:///"
								+ Environment.getExternalStorageDirectory()
										.toString() + "/screenshot.png"));
				PackageManager pm = mainActivity.getApplicationContext()
						.getPackageManager();
				List<ResolveInfo> activityList = pm.queryIntentActivities(
						shareIntent, 0);
				for (final ResolveInfo app : activityList) {
					if ((app.activityInfo.name).contains("twitter")) {
						final ActivityInfo activity = app.activityInfo;
						final ComponentName name = new ComponentName(
								activity.applicationInfo.packageName,
								activity.name);
						shareIntent.addCategory(Intent.CATEGORY_LAUNCHER);
						shareIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
								| Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED);
						shareIntent.setComponent(name);
						mainActivity.startActivity(shareIntent);
					}
				}
			}
		});

	}

	public static void shareEmail(final int m_iScore) {
		mainActivity.runOnUiThread(new Runnable() {
			@Override
			public void run() {
				Intent shareIntent = new Intent(
						android.content.Intent.ACTION_SEND);
				shareIntent.setType("image/png");
				shareIntent.putExtra(android.content.Intent.EXTRA_SUBJECT,
						"My Score Image");
				shareIntent.putExtra(android.content.Intent.EXTRA_EMAIL, "");
				shareIntent.putExtra(android.content.Intent.EXTRA_TEXT,
						"I got score " + m_iScore + " from my Jumpy Frog Game");
				shareIntent.putExtra(
						Intent.EXTRA_STREAM,
						Uri.parse("file:///"
								+ Environment.getExternalStorageDirectory()
										.toString() + "/screenshot.png"));
				PackageManager pm = mainActivity.getApplicationContext()
						.getPackageManager();
				List<ResolveInfo> activityList = pm.queryIntentActivities(
						shareIntent, 0);
				for (final ResolveInfo app : activityList) {
					if ((app.activityInfo.name).contains("email")) {
						final ActivityInfo activity = app.activityInfo;
						final ComponentName name = new ComponentName(
								activity.applicationInfo.packageName,
								activity.name);
						shareIntent.addCategory(Intent.CATEGORY_LAUNCHER);
						shareIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
								| Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED);
						shareIntent.setComponent(name);
						mainActivity.startActivity(shareIntent);
					}
				}

			}
		});

	}

	private static Bitmap SavePixels(int x, int y, int w, int h, GL10 gl) {
		int b[] = new int[w * h];
		int bt[] = new int[w * h];
		IntBuffer ib = IntBuffer.wrap(b);
		ib.position(0);
		gl.glReadPixels(x, y, w, h, GL10.GL_RGBA, GL10.GL_UNSIGNED_BYTE, ib);

		/*
		 * remember, that OpenGL bitmap is incompatible with Android bitmap and
		 * so, some correction need.
		 */
		for (int i = 0; i < h; i++) {
			for (int j = 0; j < w; j++) {
				int pix = b[i * w + j];
				int pb = (pix >> 16) & 0xff;
				int pr = (pix << 16) & 0x00ff0000;
				int pix1 = (pix & 0xff00ff00) | pr | pb;
				bt[(h - i - 1) * w + j] = pix1;
			}
		}
		Bitmap sb = Bitmap.createBitmap(bt, w, h, Bitmap.Config.ARGB_8888);
		return sb;
	}

	private static void SavePNG(int x, int y, int w, int h, String name, GL10 gl) {
		Bitmap bmp = SavePixels(x, y, w, h, gl);
		try {

			FileOutputStream fos = new FileOutputStream(Environment
					.getExternalStorageDirectory().toString() + name);
			bmp.compress(CompressFormat.PNG, 100, fos);
			try {
				fos.flush();
			} catch (IOException e) {
				e.printStackTrace();
			}
			try {
				fos.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

	}

	public static void share() {

		CCDirector.sharedDirector();
		SavePNG(0, (int) (Utils.SC_H * (2.0 / 10.0)), (int) Utils.SC_W,
				(int) (Utils.SC_H * (8.0 / 10.0)), "/screenshot.png",
				CCDirector.gl);

		mainActivity.runOnUiThread(new Runnable() {
			@Override
			public void run() {

				Intent shareIntent = new Intent(
						android.content.Intent.ACTION_SEND);
				shareIntent.setType("image/png");
				shareIntent.putExtra(android.content.Intent.EXTRA_SUBJECT,
						mainActivity.getString(R.string.app_name));
				String packageName = mainActivity.getPackageName();
				shareIntent.putExtra(android.content.Intent.EXTRA_TEXT,
						"https://play.google.com/store/apps/details?id="
								+ packageName);
				shareIntent.putExtra(
						Intent.EXTRA_STREAM,
						Uri.parse("file:///"
								+ Environment.getExternalStorageDirectory()
										.toString() + "/screenshot.png"));
				mainActivity.startActivity(Intent.createChooser(shareIntent,
						"Share using"));
			}
		});
	}

	public static void showMessageDialog(final String title,
			final String message) {
		final Activity activity = CCDirector.sharedDirector().getActivity();
		activity.runOnUiThread(new Runnable() {
			public void run() {
				android.app.AlertDialog.Builder builder = new AlertDialog.Builder(
						activity)
						.setIcon(android.R.drawable.ic_dialog_info)
						.setTitle(title)
						.setMessage(message)
						.setPositiveButton("OK",
								new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog,
											int whichButton) {
										dialog.dismiss();
									}
								});
				builder.create().show();
			}
		});
	}

	public static void shareFacebook(int m_iScore) {
		String packageName = mainActivity.getPackageName();

		FacebookDialog shareDialog = new FacebookDialog.ShareDialogBuilder(
				mainActivity)
				.setName(mainActivity.getString(R.string.app_name))
				.setLink(
						"https://play.google.com/store/apps/details?id="
								+ packageName)
				.setDescription("I have reached " + m_iScore + " balls.")
				.setPicture("http://s23.postimg.org/kn201qmt7/512x512.png")
				.build();
		shareDialog.present();
	}

	public static void vibrate(long milis) {
		Vibrator v = (Vibrator) mainActivity
				.getSystemService(Context.VIBRATOR_SERVICE);
		v.vibrate(milis);
	}
}