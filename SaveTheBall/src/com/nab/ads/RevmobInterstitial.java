package com.nab.ads;

import android.app.Activity;
import android.util.Log;

import com.revmob.RevMob;
import com.revmob.RevMobAdsListener;
import com.revmob.ads.fullscreen.RevMobFullscreen;

final class RevmobInterstitial {
	private static Activity mActivity;
	private static final String TAG = "[RevMob Interstitial]";

	private static RevMobFullscreen fullscreen;

	public static void create(Activity act) {
		mActivity = act;

		// Starting RevMob session
		// RevMob Media ID configured in the AndroidManifest.xml file
		RevMob revmob = RevMob.start(mActivity);

		// pre-load it without showing it
		fullscreen = revmob.createFullscreen(mActivity, revmobListener);
	}

	public static void onPreLoad() {
		fullscreen.load();
	}
	
	 // Invoke displayInterstitial() when you are ready to display an
	 // interstitial.
	 public static void showAd() {
		 fullscreen.show();
	 }

	private static final RevMobAdsListener revmobListener = new RevMobAdsListener() {
	    @Override
	    public void onRevMobAdDisplayed() {
	        Log.i(TAG, "onAdDisplayed");
	    }

	    @Override
	    public void onRevMobAdReceived() {
	        Log.i(TAG, "onAdReceived");
	    }

	    @Override
	    public void onRevMobAdNotReceived(String message) {
	        Log.i(TAG, "onAdNotReceived");
	    }

	    @Override
	    public void onRevMobAdDismiss() {
	        Log.i(TAG, "onAdDismiss");
	    }

	    @Override
	    public void onRevMobAdClicked() {
	        Log.i(TAG, "onAdClicked");
	    }

		@Override
		public void onRevMobEulaIsShown() {
		}

		@Override
		public void onRevMobEulaWasAcceptedAndDismissed() {
		}

		@Override
		public void onRevMobEulaWasRejected() {
		}

		@Override
		public void onRevMobSessionIsStarted() {
			
		}

		@Override
		public void onRevMobSessionNotStarted(String arg0) {
		}
	};
}
