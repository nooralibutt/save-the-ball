package com.nab.ads;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.nab.savetheball.R;

import android.app.Activity;
import android.util.Log;

final class AdMobInterstitial {

	private static Activity mActivity;
	private static final String TAG = "[Admob Interstitial]";

	private static InterstitialAd interstitial;
	private static AdRequest adRequest;

	public static void create(Activity act) {
		mActivity = act;

		// Create the interstitial.
		interstitial = new InterstitialAd(mActivity);
		interstitial.setAdUnitId(mActivity
				.getString(R.string.admob_interstitial_id));
		interstitial.setAdListener(AD_LISTENER);

		// Create ad request.
		adRequest = new AdRequest.Builder().build();
	}

	public static void onPreLoad() {
		// Begin loading your interstitial.
		interstitial.loadAd(adRequest);
	}
	
	// Invoke displayInterstitial() when you are ready to display an
	// interstitial.
	public static void showAd() {
		if (interstitial.isLoaded()) {
			interstitial.show();
		}
	}
	
	private static final AdListener AD_LISTENER = new AdListener() {
		public void onAdLoaded() {
			Log.i(TAG, "AdLoaded");
		}

		public void onAdFailedToLoad(int errorCode) {
			Log.e(TAG, "onAdFailedToLoad: " + errorCode);
		}

		public void onAdOpened() {
			Log.i(TAG, "onAdOpened");
		}

		public void onAdClosed() {
			Log.i(TAG, "onAdClosed");
		}

		public void onAdLeftApplication() {
			Log.i(TAG, "onAdLeftApplication`");
		}
	};
}
