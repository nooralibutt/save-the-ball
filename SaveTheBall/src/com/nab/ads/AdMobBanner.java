package com.nab.ads;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.nab.savetheball.R;

import android.app.Activity;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;

final class AdMobBanner {
	/** The view to show the ad. */
	private static AdView adView;
	private static Activity mActivity;
	private static final String TAG = "[Admob Banner]";

	public static void create(Activity act) {
		mActivity = act;

		// Create the adView.
		adView = new AdView(mActivity);
		adView.setAdSize(AdSize.BANNER);
		adView.setAdUnitId(mActivity.getString(R.string.admob_banner_id));
		adView.setAdListener(AD_LISTENER);
		LinearLayout layout = (LinearLayout) mActivity
				.findViewById(R.id.adView);
		layout.addView(adView);

		AdRequest adRequest = new AdRequest.Builder().build();
		adView.loadAd(adRequest);
		adView.setVisibility(View.INVISIBLE);
	}

	private static final AdListener AD_LISTENER = new AdListener() {
		public void onAdLoaded() {
			Log.i(TAG, "AdLoaded");
		}

		public void onAdFailedToLoad(int errorCode) {
			Log.e(TAG, "onAdFailedToLoad: " + errorCode);
		}

		public void onAdOpened() {
			Log.i(TAG, "onAdOpened");
		}

		public void onAdClosed() {
			Log.i(TAG, "onAdClosed");
		}

		public void onAdLeftApplication() {
			Log.i(TAG, "onAdLeftApplication`");
		}
	};
	
	public static void showAd(){
		adView.setVisibility(View.VISIBLE);
	}
	public static void hideAd(){
		adView.setVisibility(View.INVISIBLE);
	}
}
