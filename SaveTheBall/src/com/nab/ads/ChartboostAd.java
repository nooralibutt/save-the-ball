package com.nab.ads;

import android.app.Activity;
import android.util.Log;
import com.chartboost.sdk.*;
import com.chartboost.sdk.Chartboost.CBAgeGateConfirmation;
import com.chartboost.sdk.Model.CBError.CBClickError;
import com.chartboost.sdk.Model.CBError.CBImpressionError;
import com.nab.savetheball.R;

final class ChartboostAd {
	private static Chartboost cb;
	private static Activity mActivity;
	private static final String TAG = "[Chartboost]";
	private static ChartboostDelegate chartBoostDelegate = new ChartboostDelegate() {

		@Override
		public boolean shouldDisplayInterstitial(String location) {
			Log.i(TAG, "SHOULD DISPLAY INTERSTITIAL '" + location + "'?");
			return true;
		}

		@Override
		public boolean shouldRequestInterstitial(String location) {
			Log.i(TAG, "SHOULD REQUEST INSTERSTITIAL '" + location + "'?");
			return true;
		}

		@Override
		public void didCacheInterstitial(String location) {
			Log.i(TAG, "INTERSTITIAL '" + location + "' CACHED");
		}

		@Override
		public void didDismissInterstitial(String location) {
			Log.i(TAG, "INTERSTITIAL '" + location + "' DISMISSED");
		}

		@Override
		public void didCloseInterstitial(String location) {
			Log.i(TAG, "INSTERSTITIAL '" + location + "' CLOSED");
		}

		@Override
		public void didClickInterstitial(String location) {
			Log.i(TAG, "DID CLICK INTERSTITIAL '" + location + "'");
		}

		@Override
		public void didShowInterstitial(String location) {
			Log.i(TAG, "INTERSTITIAL '" + location + "' SHOWN");
		}

		@Override
		public boolean shouldDisplayLoadingViewForMoreApps() {
			return true;
		}

		@Override
		public boolean shouldRequestMoreApps() {

			return true;
		}

		@Override
		public boolean shouldDisplayMoreApps() {
			Log.i(TAG, "SHOULD DISPLAY MORE APPS?");
			return true;
		}

		@Override
		public void didCacheMoreApps() {
			Log.i(TAG, "MORE APPS CACHED");
		}

		@Override
		public void didDismissMoreApps() {
			Log.i(TAG, "MORE APPS DISMISSED");

		}

		@Override
		public void didCloseMoreApps() {
			Log.i(TAG, "MORE APPS CLOSED");
		}

		@Override
		public void didClickMoreApps() {
			Log.i(TAG, "MORE APPS CLICKED");

		}

		@Override
		public void didShowMoreApps() {
			Log.i(TAG, "MORE APPS SHOWED");
		}

		@Override
		public boolean shouldRequestInterstitialsInFirstSession() {
			return true;
		}

		@Override
		public void didFailToLoadInterstitial(String arg0,
				CBImpressionError arg1) {
		}

		@Override
		public void didFailToLoadMoreApps(CBImpressionError arg0) {
		}

		@Override
		public void didFailToRecordClick(String arg0, CBClickError arg1) {
		}

		@Override
		public boolean shouldPauseClickForConfirmation(
				CBAgeGateConfirmation arg0) {
			return false;
		}
	};

	public static void create(Activity act) {
		mActivity = act;
		cb = Chartboost.sharedChartboost();
		String appId = act.getResources().getString(R.string.chartboost_app_id);
		String appSignature = act.getResources().getString(
				R.string.chartboost_app_signature);
		cb.onCreate(act, appId, appSignature, chartBoostDelegate);
	}

	public static void onStart(Activity act) {
		cb.onStart(act);
	}

	/**
	 * Show an interstitial. This and other interstital/MoreApps cache/show
	 * calls should be used after onStart().
	 */
	public static void showAd() {
		cb.showInterstitial();
	}

	public static void onStop() {
		cb.onStop(mActivity);
	}

	public static void onPreLoad() {
		cb.cacheInterstitial();
	}

	public static void onDestroy() {
		cb.onDestroy(mActivity);
	}

	public static boolean onBackPressed() { 
	    // If an interstitial is on screen, close it. Otherwise continue as normal. 
	    if (cb.onBackPressed()) 
	        return true;
	    return false;
	}
}
