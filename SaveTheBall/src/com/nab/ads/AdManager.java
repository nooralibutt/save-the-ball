package com.nab.ads;

import android.app.Activity;

public final class AdManager {
	private static Activity act;

//	private static final int TOTAL_ADS = 3;
//	private static int currentlyShowingAd;

	public static boolean isFullScreenAdShowing = false;
	public static boolean isBannerAdShowing = false;

	public static void initialize(Activity act) {
		AdManager.act = act;

		AdMobBanner.create(act);
		ChartboostAd.create(act);
		AdMobInterstitial.create(act);
		RevmobInterstitial.create(act);
	}

	public static void onStart() {
		ChartboostAd.onStart(act);
	}

	public static void onDestroy() {
		ChartboostAd.onDestroy();
	}

	public static void onStop() {
		ChartboostAd.onStop();
	}

	public static void showBannerAd() {
		if (isBannerAdShowing) {
			AdManager.act.runOnUiThread(new Runnable() {
				@Override
				public void run() {
					AdMobBanner.showAd();
				}
			});
		}
	}

	public static boolean onBackPressed() {
		return ChartboostAd.onBackPressed();
	}

	public static void onPreLoad() {

		// +1 for not showing ads sometime
//		currentlyShowingAd = Utils.rand() % (TOTAL_ADS + 1);
		AdManager.act.runOnUiThread(new Runnable() {
			@Override
			public void run() {
//				if (currentlyShowingAd != 2)
					ChartboostAd.onPreLoad();
				// else if (currentlyShowingAd == 1)
				// AdMobInterstitial.onPreLoad();
				// else if (currentlyShowingAd == 2)
				// RevmobInterstitial.onPreLoad();
			}
		});
	}

	public static void showRandomFullScreenAd() {
		if (isFullScreenAdShowing) {
			AdManager.act.runOnUiThread(new Runnable() {
				@Override
				public void run() {
//					if (currentlyShowingAd != 2)
						ChartboostAd.showAd();
					// else if (currentlyShowingAd == 1)
					// AdMobInterstitial.showAd();
					// else if (currentlyShowingAd == 2)
					// RevmobInterstitial.showAd();
				}
			});
		}
	}

	public static void hideBannerAd() {
		AdManager.act.runOnUiThread(new Runnable() {
			@Override
			public void run() {
				AdMobBanner.hideAd();
			}
		});
	}
}
